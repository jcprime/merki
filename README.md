# merki
Command line personal health tracker

```shell
$ merki
NAME:
   merki - Command line personal health tracker

USAGE:
   merki [global options] command [command options] [arguments...]

VERSION:
   0.0.1

COMMANDS:
   add, a		Add measurement value to the file
   sparkline, spark	Draw sparkline graph for a measure
   measurements, m	Return list of all used measurements
   filter, f		Filter records for single measurement
   help, h		Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --file, -f "health.log"	Log file path [$MERKI_FILE]
   --help, -h			show help
   --version, -v		print the version
```